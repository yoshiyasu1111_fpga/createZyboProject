
@if "%1"=="" (
    set projectName=design
) else (
    set projectName=%1
)

@if "%2"=="" (
    set projectDirectory=project
) else (
    set projectDirectory=%2
)

C:\Xilinx\Vivado\2018.3\bin\vivado.bat -mode batch -source .\script\create_project.tcl -tclargs %projectName% %projectDirectory%
