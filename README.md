# 1. ZYBOプロジェクト生成Tcl

VivadoでZybo用のプロジェクトを作成するときに、GUIでポチポチするのが煩わしく感じていたので、Zybo用のプロジェクトを作成するスクリプトを作りました。

# 2. 環境

- Windows 10 pro
- Vivado 2018.3

# 3. クローン

```
> git clone git@gitlab.com:yoshiyasu1111_fpga/createZyboProject.git
```

# 4. 使用法

ZYBO-Z7-10用のボードファイルをインストールしてあることを前提としています。

## 4.1 コマンドプロンプト

```
> CreateProject.bat [プロジェクト名] [プロジェクトディレクトリ]
```

第一引数にプロジェクト名、第二引数にプロジェクトディレクトリをとります。
どちらも指定しなければデフォルトの値`プロジェクト名：design`、`プロジェクトディレクトリ：project`になります。
プロジェクト名を指定しなければプロジェクトディレクトリを指定することはできません。

## 4.2 GUI

ファイルをダブルクリックでもスクリプトを実行することができます。この場合プロジェクト名はデフォルトの`design`、プロジェクトディレクトリはデフォルトの`project`になります。

# 5. 例

```
> git clone git@gitlab.com:yoshiyasu1111_fpga/createZyboProject.git
> cd .\createZyboProject\
> .\CreateProject.bat zybo proj
```

カレントディレクトリ以下にprojディレクトリが作成され、その下にzyboプロジェクトが作成されます。
