# Create Project
create_project [lindex $argv 0] [lindex $argv 1] -part xc7z010clg400-1
set_property board_part digilentinc.com:zybo-z7-10:part0:1.0 [current_project]

# start GUI
start_gui
